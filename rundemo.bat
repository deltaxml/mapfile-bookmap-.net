echo off
cd %CD%
set path=%path%;%SystemRoot%\Microsoft.NET\Framework\v3.5
echo clearing previous output
IF EXIST results (
rmdir results /s/q
)
mkdir results
echo starting 1 of 2
..\..\bin\deltaxml-dita.exe compare mapfile in1.bookmap in2.bookmap results/dita-markup.bookmap output-format=dita-markup preservation-mode=roundTrip whitespace-processing-mode=ignore
echo starting 2 of 2
..\..\bin\deltaxml-dita.exe compare mapfile in1.bookmap in2.bookmap results/oxygen-tcs.bookmap output-format=oxygen-tcs preservation-mode=roundTrip whitespace-processing-mode=ignore
echo complete
pause
